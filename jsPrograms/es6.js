// function greet(name)
// {
//     let g;
//     if(name==='john')
//     {
//          g = "hello john"
//     }
//     else{
//          g = "hello all "
//     }
   
//     console.log(g);
// }

// greet('john')


// var a=1;
// var b=2;
// if(a===1)
// {
//     var a=10;
//     let b=20;
//     console.log(a);
//     console.log(b);
// }
// console.log(a);
// console.log(b);

let x=10;
//let x=20;

// for (let index = 0; index < 5; index++) {
//     setTimeout(()=>console.log(index),1000)
    
// }

// let num1;
// const num2=10;

// const obj1= {
//     name: 'john wick'
// }
// console.log(obj1.name)

// //obj1={};

// obj1.name="Dom Bosco"

// console.log(obj1.name)

// var sum1 =function(num1,num2)
// {
//   return num1+num2
// }

// console.log(sum1(10,20))

// var sum2 =num1=> num1*10

// console.log(sum2(10))

// var squareOfaNumber = n=>n*n;

// var employee= {
//     id: 1,
//     greet :function(){
//        // var self=this
//         setTimeout(()=>{console.log(this.id)},1000)
//     }    
// }

// employee.greet();

// let percentageBonus=0.1;
// let getValue = function(value=10,bonus=percentageBonus*value)
// {
//     console.log(value+bonus);
//     console.log(arguments.length)
// }

// getValue();
// getValue(100);
// getValue(10,20);
// getValue(undefined,100);



// let displayColors = function(message,...colours){
//    console.log(message)
//    console.log(colours)
//    for (const i in colours) {
//     console.log(colours[i]);
//    }
// }

// let message="all colours are beautiful"
// let colors =['red','blue','green']
// displayColors(message,...colors);
// displayColors(message,'red','blue');
// displayColors(message,'red');


let firstName = 'john'
let lastName ='wick'

// let person={
//     firstName,
//     lastName
// };

// console.log(person.firstName)

function createPerson(firstName,lastNAme,age)
{
    let fullName = firstName + " " + lastNAme;
    return {
        firstName,
        lastNAme,
        fullName,
        isSenior:function(){
            return age>60;
        }

    }
}

// let p = createPerson("john","wick",71)
// console.log(p.firstName)
// console.log(p.lastNAme)
// console.log(p.fullName)
// console.log(p.isSenior())

// let ln = "last Name";
// let person={
//     "first Name": 'john',
//      [ln] : 'wick'
// };

// console.log(person["first Name"])
// console.log(person["last Name"])


let employees= ['abcd','efgh','ijkl',"mnop"];

//let [ , ,emp3]=employees
let [emp1,emp2,emp3,emp4='xyz']=employees

//let[emp1,...emps]=employees 

// console.log(emp1);
// console.log(emp2);
// console.log(emp3);
// console.log(emp4)


let customer={
    fname: "abcd",
    lname:"defg",
    gender: "male"
};

let {fname:fn,lname:ln,gender:g}= customer

// console.log(fn)
// console.log(ln);
// console.log(g);

let user="saqib"
let message = `welcome ${user} 
                'to' 
              "incedo"`


console.log(message)


let colors = ['red','blue','green']

for (const index in colors) {
   console.log(colors[index])
}
for (const color of colors) {
    console.log(color)
}
let fullname = "rajarammohanroy"
for (const letter of fullname) {
    console.log(letter)
}




class Person{
  constructor(name)
{
    this.name=name;
    console.log(this.name+ " parent constructor")
}
  static staticDemoFunction()
  {
    console.log("static function")
  }
  nonstaticDemoFunction()
  {
    console.log("non static function")
  }
}

class Employee extends Person
{
  constructor(name)
  {
    super(name)
    this.name=name;
    console.log(this.name+ " child constructor")
  }
}

// let p = new Person("john")
// let e = new Employee("Dominic")
// p.nonstaticDemoFunction()
// e.nonstaticDemoFunction()
// Employee.staticDemoFunction()
// Person.staticDemoFunction()

// console.log(p)
// console.log(typeof Person)

