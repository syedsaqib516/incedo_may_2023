const http = require("node:http")
const fs = require("node:fs")
const server=http.createServer((req,res)=>{
   if(req.url==="/"){

  res.writeHead(200,{"Content-Type":"text/plain"});
  res.end("home page")
   }
   else if(req.url==="/about"){
    res.writeHead(200,{"Content-Type":"text/plain"});
    res.end("about page")
   }
   else if(req.url==="/api"){
    res.writeHead(200,{"Content-Type":"application/json"});
    res.end(JSON.stringify({firstName:"syed",lastname:"saqib"}))
   }
})

server.listen(3000,()=>{
    console.log("server started, and is listening on port 3000")
})