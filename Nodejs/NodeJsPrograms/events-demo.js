const EventEmitter = require("node:events")

const emitter=new EventEmitter()

console.log("first stmt")

emitter.on("order-pizza",(size,topping)=>{
    console.log(`order received! baking a ${size} pizza with ${topping} toppings`);

})

emitter.on("order-pizza",(size)=>{
    if(size==='large')
    console.log(`order received! free drink with ${size} pizza `);

})
console.log("second stmt")
emitter.emit("order-pizza","large","chiken");
console.log("third stmt")