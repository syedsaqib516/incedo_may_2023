// require('./module1')
// require('./module2')

// const SuperHero=require('./super-hero')
// const batman = new SuperHero("Batman")
// console.log(batman.getName());
// batman.setName("bruce wayne")
// console.log(batman.getName());

// const superman= new SuperHero("superman")
// console.log(superman.getName())
// import {add,sub} from "./math.mjs"



// console.log(add(10,20))
// console.log(add(30,20))

// const data=require("./data.json")

// console.log(data.name);

// console.log(data.address.city)

// console.log('hello universe')


//const path = require("node:path")

// console.log(__filename)
// console.log(__dirname)

// console.log(path.basename(__filename))
// console.log(path.basename(__dirname))

// console.log(path.extname(__filename))
// console.log(path.extname(__dirname))

// console.log(path.parse(__filename))
// console.log(path.format(path.parse(__filename)))

// console.log(path.isAbsolute(__filename))
// console.log(path.isAbsolute('./data.json'))

// console.log(path.join("folder1","folder2","index.html"))
// console.log(path.join("/folder1","folder2","index.html"))
// console.log(path.join("/folder1","//folder2","index.html"))
// console.log(path.join("/folder1","//folder2","../index.html"))

// console.log(path.resolve("folder1","folder2","index.html"))
// console.log(path.resolve("/folder1","folder2","index.html"))
// console.log(path.resolve("/folder1","//folder2","index.html"))
// console.log(path.resolve("/folder1","//folder2","../index.html"))//

// function greet(name)
// {
//     console.log(`hello ${name}`)
// }

// function higherOrderFn(callBackFn)
// {
//     const name= "incedo";
//     callBackFn(name);
// }

// higherOrderFn(greet);
const { error } = require("node:console");
// const fs = require("node:fs/promises");


console.log("first")
// const fileContents = fs.readFileSync("./file.txt","utf-8")
// console.log(fileContents)
// fs.readFile("./file.txt","utf-8",(error,data)=>
// {
//     if(error){
//         console.log(error)
//     }

//     else{
//         console.log(data)
//     }
// })

// console.log("second")

// fs.writeFileSync("./greet.txt","Hey there! how are you ?")

// fs.writeFile("./greet.txt","Hello there! how do you do ?",{flag:"a"},(error,data)=>
// {
//     if(error){
//         console.log(error)
//     }

//     else{
//         console.log("file written successfully")
//     }
// })

// fs.readFile("./greet.txt","utf-8")
// .then((data)=>console.log(data))
// .catch((error)=>console.log(error))



// async function readFile()
// {
//     try {
//        const data= await fs.readFile("greet.txt","utf-8")
//        console.log(data)
//     } catch (error) {
//         console.log(error);
//     }
// }
// readFile();
// console.log("second")



const fs = require("node:fs");
const readableStream =fs.createReadStream("./file.txt",{encoding:"utf-8",
highWaterMark:2})
const writeableStream=fs.createWriteStream("./greet.txt");

const zlib = require("node:zlib");
const gzip = zlib.createGzip()

readableStream.pipe(gzip).pipe(fs.WriteStream("./file2.txt.gz"));
const writableStream =fs.createWriteStream("./file2.txt");

readableStream.pipe(writableStream)

// readableStream.on("data",(chunk)=>
// {
//     console.log(chunk);
//     writeableStream.write(chunk);
// })
