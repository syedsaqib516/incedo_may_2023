const PizzaShop = require('./pizza-shop')
const pizzaShop = new PizzaShop();

pizzaShop.on("order",(size,topping)=>
{
    console.log(`order received! baking a ${size} pizza with ${topping} toppings`)
})

pizzaShop.order('large','mushroom');
pizzaShop.displayOrderNumber();